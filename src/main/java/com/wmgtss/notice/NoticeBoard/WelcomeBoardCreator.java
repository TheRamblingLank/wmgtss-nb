package com.wmgtss.notice.NoticeBoard;

import ch.qos.logback.core.encoder.EchoEncoder;
import com.wmgtss.notice.NoticeBoard.db.document.Board;
import com.wmgtss.notice.NoticeBoard.db.document.Post;
import com.wmgtss.notice.NoticeBoard.model.BoardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to create a Welcome board on spring startup
 */
@Component
public class WelcomeBoardCreator {

    private static final String WELCOME_ID = "9999";
    private static final String WELCOME = "Welcome!";
    private static final String WELCOME_POST = "You are on the WMGTSS Notice Board!";
    private static final String WELCOME_CONTENT = "<p>Try browsing for a board in the search bar</p>";

    @Autowired
    BoardRepository boardRepository;

    /**
     * Checks for and creates the welcome board, if necessary, on start up of Spring Boot
     */
    @EventListener(ApplicationReadyEvent.class)
    public void onStartUp() {
        if (boardRepository.findBoardById(WELCOME_ID) == null) {
            Board welcome = new Board(WELCOME);
            List<Post> postList = new ArrayList<>();
            postList.add(new Post(WELCOME_POST, WELCOME_CONTENT, true));
            welcome.setPinnedPosts(postList);
            welcome.setId(WELCOME_ID);
            boardRepository.insert(welcome);
        }
    }

}
