package com.wmgtss.notice.NoticeBoard.db.document;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

/**
 * Post DTO
 */
@Data
@RequiredArgsConstructor
public class Post {

    @Id
    private String id;

    @NonNull
    @Field("post_title")
    private String postTitle;

    @NonNull
    @Field("post_content")
    private String postContent;

    private List<Comment> comments;

    private int likes;

    @NonNull
    private boolean pinned;

}
