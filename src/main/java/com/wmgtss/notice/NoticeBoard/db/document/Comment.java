package com.wmgtss.notice.NoticeBoard.db.document;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Comment DTO
 */
@Data
public class Comment {

    @Id
    private String id;

    @Field("comment_content")
    private String commentContent;

    private int likes;
}
