package com.wmgtss.notice.NoticeBoard.db.document;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

/**
 * Board DTO
 */
@Data
@Document(collection = "boards")
@RequiredArgsConstructor
public class Board {

    @Id
    private String id;

    @NonNull
    @Field("board_name")
    private String boardName;

    private List<Post> posts;

    @Field("pinned_posts")
    private List<Post> pinnedPosts;

}
