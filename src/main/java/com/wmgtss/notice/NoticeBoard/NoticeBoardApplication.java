package com.wmgtss.notice.NoticeBoard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Spring boot Main
 */
@SpringBootApplication
@EnableMongoRepositories({"com.wmgtss.notice.NoticeBoard.model"})
public class NoticeBoardApplication {

	public static void main(String[] args) {
		SpringApplication.run(NoticeBoardApplication.class, args);
	}

}
