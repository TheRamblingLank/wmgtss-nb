package com.wmgtss.notice.NoticeBoard.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Collections;

@Slf4j
@Controller
@CrossOrigin(origins = "http://localhost:3000")
public class ReactAppController {

    /**
     * Serve index.html
     * @return index.html
     */
    @RequestMapping(value={"/", "/home"})
    public String sendUI() {
        return "forward:/index.html";
    }

    /**
     * Listens for requests to log-in.
     * @return token if verified
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> userRequestsLogin() {
        log.info("login...");
        String token = "a_token";
        return ResponseEntity.ok(Collections.singleton(token));
    }
}
