package com.wmgtss.notice.NoticeBoard.controller;

import com.google.gson.Gson;
import com.wmgtss.notice.NoticeBoard.db.document.Board;
import com.wmgtss.notice.NoticeBoard.db.document.Post;
import com.wmgtss.notice.NoticeBoard.model.BoardRepository;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Controller between the view (React app) and model (repository)
 */
@Slf4j
@Controller
@CrossOrigin(origins = "http://localhost:3000")
public class ApiController {

    /**
     * This is the repository that controls access to the database
     */
    @Autowired
    BoardRepository boardRepository;

    private static final String ID = "id";
    private static final String TITLE = "title";
    private static final String POST_CONTENT = "post-content";
    private static final String PINNED = "pinned";

    /**
     * Gets all available boards and processes them into usable JSON for the search bar
     * @return boards in JSON
     */
    @RequestMapping(value = "/api/populate-nav-select", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> populateNavSelect() {
        List<Board> boards = boardRepository.findAll();
        JSONArray array = new JSONArray();
        // Process the JSON into a format understood by the frontend
        for (Board board : boards) {
            JSONObject json = new JSONObject();
            json.put("value", board.getId());
            json.put("label", board.getBoardName());
            array.appendElement(json);
        }
        return ResponseEntity.ok(array.toJSONString());
    }

    /**
     * Find one board by ID
     * @param payLoad the ID
     * @return the board in JSON
     */
    @RequestMapping(value = "/api/get-board-by-id", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getBoardById(@RequestBody Map<String, String> payLoad) {
        Board board = boardRepository.findBoardById(payLoad.get(ID).replaceAll("^\"|\"$", ""));
        Gson gson = new Gson();
        return ResponseEntity.ok(gson.toJson(board));
    }

    /**
     * Inserts a board into the DB
     * @param payload the new board information as JSON
     * @return Ok response
     */
    @RequestMapping(value = "/api/insert-board", method = RequestMethod.POST)
    public ResponseEntity<?> insertBoard(@RequestBody Map<String, String> payload) {
        Board board = new Board(payload.get("board-name"));
        boardRepository.insert(board);
        return ResponseEntity.ok("ok");
    }

    /**
     * Updates a given board with a new post
     * @param payload the new post and the ID of the board
     * @return ok response
     */
    @RequestMapping(value = "/api/insert-post", method = RequestMethod.POST)
    public ResponseEntity<?> insertPost(@RequestBody Map<String, String> payload) {
        // Get the board first
        Board board = boardRepository.findBoardById(payload.get(ID).replaceAll("^\"|\"$", ""));
        if (Boolean.parseBoolean(payload.get(PINNED))) {
            // The post is pinned
            List<Post> posts = board.getPinnedPosts();
            if (posts == null) {
                posts = new ArrayList<>();
            }
            posts.add(new Post(payload.get(TITLE), payload.get(POST_CONTENT), true));
            board.setPinnedPosts(posts);
        } else {
            // Post is not pinned
            List<Post> posts = board.getPosts();
            if (posts == null) {
                posts = new ArrayList<>();
            }
            posts.add(new Post(payload.get(TITLE), payload.get(POST_CONTENT), false));
            board.setPosts(posts);
        }
        boardRepository.save(board);
        return ResponseEntity.ok("ok");
    }
}
