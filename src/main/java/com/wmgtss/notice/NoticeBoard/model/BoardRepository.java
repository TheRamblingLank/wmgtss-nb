package com.wmgtss.notice.NoticeBoard.model;

import com.wmgtss.notice.NoticeBoard.db.document.Board;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Mongo Repository to access the board collection
 */
public interface BoardRepository extends MongoRepository<Board, String> {

    List<Board> findByBoardName(String boardName);
    Board findBoardById(String boardId);
}
