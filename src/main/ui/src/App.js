import MainPage from "./components/MainPage/MainPage";
import Login from "./components/Login/Login";
import './App.css';
import React, {useEffect, useState} from "react";
import {
    Routes,
    Route,
    Navigate,
    useNavigate
} from 'react-router-dom'
import NavigationBar from "./components/NavigationBar/NavigationBar";
import CreateBoard from "./components/CreateBoard/CreateBoard";
import CreatePost from "./components/CreatePost/CreatePost";

// Main App component
function App() {
    // Set some variables
    const [token, setToken] = useState();
    // Stores the ID of the open board
    const [boardId, setBoardId] = useState();
    // Stores the currently open board
    const [openBoard, setOpenBoard] = useState({});
    // Stores whether the new board dialog should be displayed
    const [newBoardToggled, setNewBoardToggled] = useState(false);
    // Stores whether the new post dialog should be displayed
    const [newPostToggled, setNewPostToggled] = useState(false);
    // Stores the select user privilege
    const [userPrivileged, setUserPrivileged] = useState(false);
    // Used to navigate to a new route when needed
    const navigate = useNavigate();

    //  Check if there is a cached board in the browser storage
    useEffect(() => {
        const cachedBoard = checkForStoredBoard();
        setBoardId(cachedBoard);
    }, [])

    // Track changes to the token and reacts when needed
    useEffect(() => {
        if (token) navigate('/home');
    }, [token]);

    // Fetch the board information whenever the ID is updated by a user making a navigation selection
    useEffect(() => {
        if (typeof boardId === "undefined") {
            return;
        }
        fetch('http://localhost:8080/api/get-board-by-id', {
            method: 'POST',
            headers: {
                'content-type': 'application/json',
            },
            body: JSON.stringify({'id': boardId})
        }).then(data => data.json())
            .then(data => setOpenBoard(data));
    }, [boardId]);

    // Store the board in local storage
    useEffect(() => {
        storeBoard(openBoard)
    }, [openBoard])

    // Check if there is a board in local browser storage
    function checkForStoredBoard() {
        try {
            const serialisedState = localStorage.getItem("board");
            if (serialisedState === null) {
                return {};
            }
            return JSON.parse(serialisedState);
        } catch (err) {
            return {};
        }
    }

    // Stores the board in local storage
    function storeBoard(toStore) {
        try {
            const serialisedState = JSON.stringify(toStore.id);
            localStorage.setItem('board', serialisedState);
        } catch (err) {
            return undefined;
        }
    }

    // The HTML. The router is defined in index.js.
    // This HTML contains Routes and Route component that manipulate what is displayed based on the route in the browser
    // navigation bar.
    return (
        <div className="App">
            <Routes>
                <Route path="/" element={<Navigate to="/loginscreen"/>}/>
                <Route path="/loginscreen" element={<div><Login setToken={setToken}/></div>}/>
                <Route path="/home" element={<div>
                    <NavigationBar setBoardId={setBoardId} toggleBoard={setNewBoardToggled}
                                   toggleUser={setUserPrivileged} />
                    <MainPage selectedBoard={openBoard} togglePost={setNewPostToggled} userPrivileged={userPrivileged}/>
                    {
                        newBoardToggled ? <CreateBoard setOpen={setNewBoardToggled}/> : null
                    }
                    {
                        newPostToggled ? <CreatePost setOpen={setNewPostToggled} boardId={boardId}/> : null
                    }
                </div>}/>
            </Routes>
        </div>
    );
}

export default App;
