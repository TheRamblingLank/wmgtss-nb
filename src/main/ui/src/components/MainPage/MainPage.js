import './MainPage.css'
import {useEffect, useState} from "react";
import Post from "../Post/Post";

// Main page to contain posts component
export default function MainPage({selectedBoard, togglePost, userPrivileged}) {

    // Define constants
    const [posts, setPosts] = useState();
    const [pinnedPosts, setPinnedPosts] = useState();

    // Sets the togglePost to true if the user selects to create a post
    function handleClick() {
        togglePost(true);
    }

    // Tries to display posts when the component renders
    useEffect(() => {
            try {
                setPosts(selectedBoard.posts);
                setPinnedPosts(selectedBoard.pinnedPosts);
            } catch
                (error) {
                //
            }
        }
    )

    // HTML
    // The lines containing in-line JS check that posts are present before attempting to display them
    return (
        <div className="mainPage">
            {
                userPrivileged ?
                    <button className="createBoardButton" onClick={handleClick}>Create post...</button> : null
            }
            <h1>{selectedBoard.boardName}</h1>

            {
                pinnedPosts ? <h1 className="post-header">Pinned</h1> : null
            }

            <div className="pinned-posts">
                {
                    pinnedPosts ? pinnedPosts.map(({postId, postTitle, postContent}) => (
                        <Post key={postId} postTitle={postTitle} postContent={postContent}/>
                    )) : null
                }
            </div>

            {
                posts ? <h1 className="post-header">Posts</h1> : null
            }
            <div className="posts-container">
                {
                    posts ? posts.map(({postId, postTitle, postContent}) => (
                        <Post key={postId} postTitle={postTitle} postContent={postContent}/>
                    )) : null
                }
            </div>
        </div>
    );
}