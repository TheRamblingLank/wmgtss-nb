import "./CreateBoard.css"
import {useState} from "react";

// Create Board pop-up component
export default function CreateBoard({setOpen}) {

    // Define constants
    const [newBoardName, setNewBoardName] = useState();

    // Submit data for new board
    function submit() {
        fetch("http://localhost:8080/api/insert-board", {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify({"board-name": newBoardName})
        }).then((response => {
            if (response.ok) {
                window.location.reload();
            } else {
                // Error handling will go here...
            }
        }));
    }

    // Close pop-up when needed
    function handleClose() {
        setOpen(false);
    }

    // HTML
    return (
        <div className="createBoardWindow">
            <h1>Create board...</h1>
            <div>
                <label className="boardNameLabel">Board name:</label>
                <input
                    type="text"
                    name="name"
                    onChange={e => setNewBoardName(e.target.value)}/> <br/>
            </div>
            <div>
                <button className="submitButton" onClick={submit}>Submit</button>
                <button className="cancel-button" onClick={handleClose}>Cancel</button>
            </div>
        </div>
    )
}