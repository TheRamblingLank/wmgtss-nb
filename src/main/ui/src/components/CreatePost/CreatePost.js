import "./CreatePost.css"
import {useState} from "react"
import ReactQuill from "react-quill"
import 'react-quill/dist/quill.snow.css'

// Create Post pop-up component
export default function CreatePost({setOpen, boardId}) {

    // Define constants
    const notPinnedMessage = "Not pinned";
    const pinnedMessage = "Pinned";
    const [postTitle, setTitle] = useState("");
    const [content, setContent] = useState("");
    const [pinButton, setPinButton] = useState(notPinnedMessage);
    const [pinned, setPinned] = useState(false);

    // Submit the new post data to the API
    function submit() {
        fetch("http://localhost:8080/api/insert-post", {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify({
                'id': boardId,
                'title': postTitle,
                'post-content': content,
                'pinned': pinned
            })
        }).then((response => {
            if (response.ok) {
                window.location.reload();
            } else {
                // Error handling will go here...
            }
        }));
    }

    // Close when needed
    function handleClose() {
        setOpen(false);
    }

    // Retain value when user inputs data
    function handleChange(value) {
        setContent(value);
    }

    // Retain if user opted to pin the post
    function setPinStatus() {
        if (!pinned) {
            setPinned(true);
            setPinButton(pinnedMessage)
        } else {
            setPinned(false);
            setPinButton(notPinnedMessage)
        }
    }

    // HTML
    return (
        <div className="create-post-window">
            <h1>Create a post</h1>
            <div className="create-post-inner">
                <div className="title-container">
                    <label className="post-title">Post Title:</label>
                    <input
                        className="title-input"
                        type="text"
                        onChange={e => setTitle(e.target.value)}/>
                </div>
                <ReactQuill className="entry-area"
                    theme="snow"
                    value={content}
                    onChange={handleChange}
                />
                <div>
                    <button className="submit-post" onClick={submit}>Submit</button>
                    <button className="pin-post" onClick={setPinStatus}>{pinButton}</button>
                    <button className="cancel-post" onClick={handleClose}>Cancel</button>
                </div>
            </div>
        </div>
    )

}
