import "./NavigationBar.css"
import Select from "react-select";
import {useEffect, useState} from "react";

// Navigation bar component
export default function NavigationBar({setBoardId, toggleBoard, toggleUser}) {

    // Define constants
    const [options, setOptions] = useState({});
    const [selectedOption] = useState();
    const [showButton, setShowButton] = useState(false);

    // Attempt to populate the lists of boards when the component renders
    useEffect(() => {
            fetch('http://localhost:8080/api/populate-nav-select')
                .then(response => response.json())
                .then(response => setOptions(response))
        }, []
    )

    // Set the ID when the user makes a selection in the search bar
    function handleChange(selectedOption) {
        setBoardId(selectedOption.value);
    }

    // Notifies the prop when the user opts to create a new post
    function boardClicked() {
        toggleBoard(true);
    }

    // Tracks what mode the user is in, true = Tutor view
    function updateUser(value) {
        if (value === "true") {
            toggleUser(true);
            setShowButton(true);
        } else {
            toggleUser(false);
            setShowButton(false);
        }
    }

    // HTML
    // The select Component is a third party library
    return (
        <nav className="nav">
            <h1 className="navigationTitle">TSS Notice Board</h1>
            <select className="role-select" onChange={e => updateUser(e.target.value)}>
                <option value="false">Student</option>
                <option value="true">Tutor mode</option>
            </select>
            {
                showButton ? <button className="createBoardNavButton" onClick={boardClicked}>Create board...</button> : null
            }
            <Select className="selection"
                    value={selectedOption}
                    onChange={handleChange}
                    options={options}
            />
        </nav>
    )
}
