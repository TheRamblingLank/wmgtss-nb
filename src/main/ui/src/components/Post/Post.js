import './Post.css'

// Reusable Post component to display user content
export default function Post({postTitle, postContent}) {

    // HTML
    return (
        <div className="post">
            <div className="text-container">
                <h1 className="title-post">{postTitle}</h1>
                <div className="post-content" dangerouslySetInnerHTML={{__html: postContent}}/>
            </div>
            <div className="post-bottom-components">
                <button>Like!</button>
                <p className="post-likes">5</p>
                <button>Comments...</button>
            </div>
        </div>
    )

}
