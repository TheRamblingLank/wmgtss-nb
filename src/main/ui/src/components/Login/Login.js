import React, {useState} from "react";
import PropTypes from 'prop-types';

// Login page component
export default function Login({setToken}) {
    // Create variables to store user details
    const [username, setUsername] = useState();
    const [password, setPassword] = useState();

    // Submit function, makes rest request to the spring application
    const submit = async e => {
        e.preventDefault();
        const token = await fetch('http://localhost:8080/login', {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(username, password)
        // This line will trigger the useEffect in App.js
        }).then(data => setToken(data))
    }

    // HTML
    return (
        <div className="loginScreen">
            <form onSubmit={submit}>
                <h1>Login:</h1>
                <label>Username: </label>
                <input
                    type="text"
                    name="username"
                    onChange={e => setUsername(e.target.value)}/><br/>
                <label>Password: </label>
                <input
                    type="text"
                    name="password"
                    onChange={e => setPassword(e.target.value)}/> <br/>
                <input
                    type="submit"
                    value="Go!"/>
            </form>
        </div>
    );
}

Login.propTypes = {
    setToken: PropTypes.func.isRequired
}
