package com.wmgtss.notice.NoticeBoard.controller;

import static org.assertj.core.api.Assertions.assertThat;

import com.wmgtss.notice.NoticeBoard.db.document.Board;
import com.wmgtss.notice.NoticeBoard.model.BoardRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;

import java.util.*;

/**
 * Mockito test for API controller
 */
@SpringBootTest
class ApiControllerTest {

    /** The target of the test */
    @Autowired
    ApiController target;

    /** Mocked Model */
    @MockBean
    private BoardRepository mockModel;

    private final String id = "b123";

    /** Set-up prior to each test */
    @BeforeEach
    public void setUp() {
        // Create a board
        Board board = new Board("Board");
        board.setId(id);
        // Add board to list
        List<Board> boards = new ArrayList<>(List.of(board));
        // Mock method returns for the model
        Mockito.when(mockModel.findAll()).thenReturn(boards);
        Mockito.when(mockModel.findBoardById(board.getId())).thenReturn(board);
    }

    @Test
    public void shouldFindAll() {
        // When...
        ResponseEntity<?> result = target.populateNavSelect();

        // Expect...
        assertThat((String) result.getBody()).contains("Board");
    }

    @Test
    public void shouldFindById() {
        // Test-specific setup
        Map<String, String> payload = new HashMap<>();
        payload.put("id", id);

        // When...
        ResponseEntity<?> result = target.getBoardById(payload);

        // Expect...
        assertThat((String) result.getBody()).contains("Board");
    }
}
