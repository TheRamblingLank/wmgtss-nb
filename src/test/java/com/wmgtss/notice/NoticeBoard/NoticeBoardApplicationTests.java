package com.wmgtss.notice.NoticeBoard;

import static org.assertj.core.api.Assertions.assertThat;

import com.wmgtss.notice.NoticeBoard.controller.ApiController;
import com.wmgtss.notice.NoticeBoard.controller.ReactAppController;
import com.wmgtss.notice.NoticeBoard.model.BoardRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

/**
 * Mockito test for Spring Boot Application
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class NoticeBoardApplicationTests {

    /** Test targets several Components */
    @Autowired
    ApiController targetApiController;

    @Autowired
    ReactAppController targetReactAppController;

    @Autowired
    BoardRepository targetModel;

    /** Library provides a rest template */
    @Autowired
    private TestRestTemplate testRestTemplate;

    @LocalServerPort
    private int port;

    /** Test that spring injects classes properly */
    @Test
    void contextLoads() {
        assertThat(targetApiController).isNotNull();
        assertThat(targetReactAppController).isNotNull();
        assertThat(targetModel).isNotNull();
    }

    /** Test web layer of Spring */
    @Test
    public void shouldReturnToken() {
        assertThat(testRestTemplate.getForObject("http://localhost:" + port + "/",
                String.class)).contains("html");
    }

}
