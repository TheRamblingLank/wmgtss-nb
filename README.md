# Welcome to the Warwick Manufacturing Group Teaching Support System Notice Board Repo!

This is a Spring Boot project serving a Nodejs frontend. It is reccomended that you view this README in a browser! It is
available [on GitLab.](https://gitlab.com/TheRamblingLank/wmgtss-nb) 

## Contents

1. [Introduction](#intro)
2. [Tools and Versions](#tools)
3. [Installation Instructions](#installation)
    1. [Java](#java)
        1. [Java Installation](#java-installation)
        2. [Add Java to PATH](#java-path)
    2. [Maven](#maven)
        1. [Maven Installation](#maven-installation)
        2. [Add Maven to PATH](#maven-path)
    3. [Node](#node)
        1. [Node Installation](#node-installation)
    4. [MongoDb](#mongodb)
       1. [MongoDb Installation](#mongodb-installation)
    5. [WMGTSS Notice Board](#nb)
4. [Running](#running)

## <u>Introduction</u> <a name="intro"></a>
This README contains the instructions on how to install all the dependencies to run the Application, as well as the
Application itself. Please ensure to follow every step. If there is an issue, this repo contains a "demo" folder 
containing images showing the software working.

## <u>Tools and Versions</u> <a name="tools"></a>

- Java Development Kit 17.0.2
- Apache Maven 3.8.4
- Node Js 16.13.2
- MongoDb 5.0.5

## <u>Installation instructions</u> <a name="installation" />

```
Warning! These instructions will only work in a Windows 64-Bit environment!
```

Please ensure to follow these instructions exactly as they are laid out. Even if you have versions of the requirements
installed, please install the ones specified above. Installing on a clean environment (E.g. a VM like Windows Sandbox)
is preferable to avoid conflicting dependencies.

### <u>Java</u> <a name="java"></a>

#### Installation <a name="java-installation"></a>

Download the 64-Bit JDK (Java Development Kit)
from [here.](https://download.oracle.com/java/17/latest/jdk-17_windows-x64_bin.exe)

Install the JDK with default settings.

#### Add to PATH <a name="java-path"></a>

Type "Environment" into the Windows search and select "Edit the System environment variables": ![Environment Variables](./images/env_vars.png)

Select "Environment Variables...", then from the "System Variables" (lower) panel, select "Path", then "Edit". From
here, select "New", then insert the location of the /bin/ directory of the Java install, usually:
C:\Program Files\Java\jdk-17.0.2\bin. Then, move it up to the top of the list: ![Adding Java to PATH](./images/java_path.png)

To Verify your installation, open a Command Prompt window, and run:

```shell
java -version
```

It should output version information.

### <u>Maven</u> <a name="maven"></a>

#### Installation <a name="maven-installation"></a>

Download Maven from [here.](https://dlcdn.apache.org/maven/maven-3/3.8.4/binaries/apache-maven-3.8.4-bin.zip)

Go to your download folder, right-click the downloaded file and select "Extract all...": ![Extract Zip](./images/extract_mvn.png)

Extract the file, a new unzipped folder will appear. Open the folder, and cut or copy the folder inside
(apache-maven-3.8.4) to a convenient location, like on the C:\ drive.

#### Add to PATH <a name="maven-path"></a>

Type "Environment" into the Windows search and select "Edit the System environment variables": ![Environment Variables](./images/env_vars.png)

Select "Environment Variables...", then from the "System Variables" (lower) panel, select "Path", then "Edit". From
here, select "New", then insert the location of the /bin/ directory of the Maven install, usually:
C:\<YOUR INSTALL LOCATION>\apache-maven-3.8.4\bin. Then, move it up to the top of the list: ![Add Maven to PATH](./images/mvn-path.png)

To verify your installation, open a _new_ Command Prompt (this will refresh the PATH changes) and run:

```shell
mvn
```

It should output the following: ![Mvn output](./images/mvn_output.png)

### <u>Node</u> <a name="node"></a>

#### Node Installation <a name="node-installation"></a>

Download NodeJs from [here.](https://nodejs.org/dist/v16.13.2/node-v16.13.2-x64.msi)

Run the installer with default options.

To verify your installation, open a new Command Prompt and run:

```shell
npm -v
```

It should output "8.1.2".

### <u>MongoDb</u> <a name="mongodb"></a>

#### MongoDb Installation <a name="mongodb-installation"></a>

Download MongoDb from [here.](https://fastdl.mongodb.org/windows/mongodb-windows-x86_64-5.0.5-signed.msi)

Run the installer with default options, ensuring to select "Complete" installation: ![Complete Install](./images/mongo-complete.png)

_Please note that installation may take some time._

Once the installation is complete, MongoDb Compass should open, if not open it from the desktop or start menu. Dismiss
any new feature dialogs, and accept the Privacy Settings. To test your installation, click the green "Connect" button,
and it will connect to your local instance with default settings, and should display this: ![Mongo Dashboard](./images/mongo-dashboard.png)

### WMGTSS Notice Board <a name="nb"></a>

Once parts i, ii, iii and iv are complete, clone or download the repo [here:](https://gitlab.com/TheRamblingLank/wmgtss-nb)
![Gitlab](./images/clone-or-download.png)

If you choose to download the repo, download it in .zip format, and extract the contents by right-clicking it.

Open a command prompt, and navigate to the repo, for example:
```shell
cd c:\repos\wmgtss-nb-main
```

To build the project run:
```shell
mvn clean install
```

Allow the command to run, a successful build will finish with: ![Built](./images/build-success.png)

At this point, the project is installed.

## Running the Application <a name="running"></a>

To run the application, follow the [Installation Instructions.](#installation) After this, open a Command Prompt and
navigate to the build output "Target" folder, for example:
```shell
cd c:\repos\wmgtss-nb-main\target
```
To start the server, run the following command, replacing the version number with the jar inside the target folder.
```shell
java -jar Notice-Board-<VERSION>-SNAPSHOT.jar
```

To connect, open a browser and insert "http://localhost:8080" into the address bar. You may have to dismiss a Windows
Security pop-up.
